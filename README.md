# Clone
```
git clone --recursive https://gitlab.com/fredloreaud/osef-smbus.git
```
all following notes will assume you cloned this repo then cd to its root
# Build library
## CMake
```
./build_release.sh
```
## NetBeans
### Local host
* Open project osef-smbus/NetBeans/OSEF_SMBus
* Right-click project > Build
### Remote host
* Open project osef-smbus/NetBeans/OSEF_SMBus
* Right-click project > Set build host > [YourRemoteHost]
* Right-click project > Build
#### Remote host addition
* Right-click project > Build > Set build host > Manage hosts ... > Add ...
  * Type remote host IP address as Hostname
* Next
  * Change login to root
* Next
  * Enter password
* OK
* Change Access project files via:to SFTP
* Finish
