#include "SMBusDevice.h"
#include "Debug.h"
#include "Swap.h"

#include <fcntl.h>  // O_RDWR
#include <sys/ioctl.h>  // ioctl
#include <unistd.h>  // close

extern "C"
{
    #include <i2c/smbus.h>  // i2c_smbus_read_i2c_block_data
}

#include  <linux/i2c.h>  // I2C_FUNC_I2C
#include  <linux/i2c-dev.h>  // I2C_SLAVE

//    #define I2C_SLAVE 0x0703
//    #define I2C_FUNCS 0x0705
//    #define I2C_SMBUS 0x0720

//    #define I2C_FUNC_I2C                    0x00000001
//    #define I2C_FUNC_10BIT_ADDR             0x00000002
//    #define I2C_FUNC_PROTOCOL_MANGLING      0x00000004 /* I2C_M_{REV_DIR_ADDR,NOSTART,..} */
//    #define I2C_FUNC_SMBUS_PEC              0x00000008
//    #define I2C_FUNC_SMBUS_BLOCK_PROC_CALL  0x00008000 /* SMBus 2.0 */
//    #define I2C_FUNC_SMBUS_QUICK            0x00010000
//    #define I2C_FUNC_SMBUS_READ_BYTE        0x00020000
//    #define I2C_FUNC_SMBUS_WRITE_BYTE       0x00040000
//    #define I2C_FUNC_SMBUS_READ_BYTE_DATA   0x00080000
//    #define I2C_FUNC_SMBUS_WRITE_BYTE_DATA  0x00100000
//    #define I2C_FUNC_SMBUS_READ_WORD_DATA   0x00200000
//    #define I2C_FUNC_SMBUS_WRITE_WORD_DATA  0x00400000
//    #define I2C_FUNC_SMBUS_PROC_CALL        0x00800000
//    #define I2C_FUNC_SMBUS_READ_BLOCK_DATA  0x01000000
//    #define I2C_FUNC_SMBUS_WRITE_BLOCK_DATA 0x02000000
//    #define I2C_FUNC_SMBUS_READ_I2C_BLOCK   0x04000000 /* I2C-like block xfer  */
//    #define I2C_FUNC_SMBUS_WRITE_I2C_BLOCK  0x08000000 /* w/ 1-byte reg. addr. */

constexpr uint8_t SMBUS_WRITE = 0;
constexpr uint8_t SMBUS_READ = 1;

constexpr uint32_t SMBUS_QUICK = 0;
constexpr uint32_t SMBUS_BYTE =  1;
constexpr uint32_t SMBUS_BYTE_DATA  = 2;
constexpr uint32_t SMBUS_WORD_DATA =  3;
// constexpr uint32_t SMBUS_PROC_CALL =  4;
constexpr uint32_t SMBUS_BLOCK_DATA = 5;
constexpr uint32_t SMBUS_I2C_BLOCK_BROKEN = 6;
// constexpr uint32_t SMBUS_BLOCK_PROC_CALL =  7;
constexpr uint32_t SMBUS_I2C_BLOCK_DATA =   8;

OSEF::SMBusDevice::SMBusDevice(const uint8_t& adp, const uint8_t& dvc)
    :file(-1),
    deviceConnected(false),
    data(),
    adapter(adp),
    device(dvc),
    functions(0)
{
    if (not connectDevice())
    {
        DERR("error connecting device");
    }

//    std::string filename = "/dev/i2c-";
//    filename += std::to_string(adapter);
//
//    file = open(filename.c_str(), O_RDWR);
//    if (file >= 0)
//    {
//        if (ioctl(file, I2C_SLAVE, device) >= 0)
//        {
//            deviceConnected = true;
//        }
//        else
//        {
//            SF_SMER("error setting device address");
//        }
//    }
//    else
//    {
//        SF_SMER("error opening SMBus file " << filename);
//    }
}

OSEF::SMBusDevice::~SMBusDevice()
{
    if (file >= 0)
    {
        close(file);
    }
}

bool OSEF::SMBusDevice::connectDevice()
{
    if (not deviceConnected)
    {
        std::string filename = "/dev/i2c-";
        filename += std::to_string(adapter);

        file = open(filename.c_str(), O_RDWR | O_CLOEXEC);
        if (file >= 0)
        {
            if (ioctl(file, I2C_SLAVE, device) >= 0)
            {
                deviceConnected = true;
                DHEX("successfully connected device 0x" << static_cast<uint32_t>(device) << " on adapter 0x" << static_cast<uint32_t>(adapter));
            }
            else
            {
                close(file);
                SF_SMER("error setting device address");
            }
        }
        else
        {
            SF_SMER("error opening SMBus file " << filename);
        }
    }
    else
    {
        DHEX("device 0x" << static_cast<uint32_t>(device) << " already connected on adapter 0x" << static_cast<uint32_t>(adapter));
    }

    return deviceConnected;
}

bool OSEF::SMBusDevice::displayFunctions(uint32_t& f)
{
    bool ret = false;

    if (connectDevice())
    {
        if (ioctl(file, I2C_FUNCS, &functions) >= 0)
        {
            std::cout << std::hex << "available functions 0x" << functions << std::dec << std::endl;

            for (uint32_t i=0x01; i <= 0x08U; i <<= 1)
            {
                displayFunction(i);
            }

            for (uint32_t i=0x00008000; i <= 0x08000000U; i <<= 1)
            {
                displayFunction(i);
            }

            f = functions;
            ret = true;
        }
        else
        {
            SF_SMER("error retrieving functions");
        }
    }

    return ret;
}

void OSEF::SMBusDevice::displayFunction(const uint32_t& f)
{
    if (checkFunction(f))
    {
        std::cout << getFunctionName(f) << " [\u2713]" << std::endl;
    }
    else
    {
        std::cout << getFunctionName(f) << " [\u274C]" << std::endl;
    }
}

bool OSEF::SMBusDevice::checkFunction(const uint32_t& f) const
{
    const bool ret = (functions & f) != 0U;
    return ret;
}

std::string OSEF::SMBusDevice::getFunctionName(const uint32_t& f)
{
    std::string ret;

    switch (f)
    {
        case I2C_FUNC_I2C:
            ret ="I²C";
            break;
        case I2C_FUNC_10BIT_ADDR:
            ret ="10 bits addresses";
            break;
        case I2C_FUNC_PROTOCOL_MANGLING:
            ret ="protocol mangling";
            break;
        case I2C_FUNC_SMBUS_PEC:
            ret ="PEC";
            break;
        case I2C_FUNC_SMBUS_BLOCK_PROC_CALL:
            ret ="block process call";
            break;
        case I2C_FUNC_SMBUS_QUICK:
            ret = "read byte quick";
            break;
        case I2C_FUNC_SMBUS_READ_BYTE:
            ret = "read byte";
            break;
        case I2C_FUNC_SMBUS_WRITE_BYTE:
            ret = "write byte";
            break;
        case I2C_FUNC_SMBUS_READ_BYTE_DATA:
            ret = "read byte data";
            break;
        case I2C_FUNC_SMBUS_WRITE_BYTE_DATA:
            ret = "write byte data";
            break;
        case I2C_FUNC_SMBUS_READ_WORD_DATA:
            ret = "read word";
            break;
        case I2C_FUNC_SMBUS_WRITE_WORD_DATA:
            ret = "write word";
            break;
        case I2C_FUNC_SMBUS_PROC_CALL:
            ret ="process call";
            break;
        case I2C_FUNC_SMBUS_READ_BLOCK_DATA:
            ret = "read block";
            break;
        case I2C_FUNC_SMBUS_WRITE_BLOCK_DATA:
            ret = "write block";
            break;
        case I2C_FUNC_SMBUS_READ_I2C_BLOCK:
            ret = "read i2c block";
            break;
        case I2C_FUNC_SMBUS_WRITE_I2C_BLOCK:
            ret = "write i2c block";
            break;
        default:
            ret = "unknown function " + std::to_string(f);
            break;
    }

    return ret;
}

bool OSEF::SMBusDevice::read(uint8_t command, uint32_t size)
{
    bool ret = false;

    if (connectDevice())
    {
        struct i2c_smbus_ioctl_data args {};
        args.read_write = SMBUS_READ;
        args.command = command;
        args.size = size;
        args.data = &data;

        if (ioctl(file, I2C_SMBUS, &args) == 0)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::SMBusDevice::write(uint8_t command, uint32_t size, union i2c_smbus_data* d)
{
    bool ret = false;

    if (connectDevice())
    {
        struct i2c_smbus_ioctl_data args {};
        args.read_write = SMBUS_WRITE;
        args.command = command;
        args.size = size;
        args.data = d;

        if (ioctl(file, I2C_SMBUS, &args) == 0)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::SMBusDevice::writeQuick(uint8_t value)
{
    bool ret = false;

    if (connectDevice())
    {
        struct i2c_smbus_ioctl_data args {};
        args.read_write = value;
        args.command = 0;
        args.size = SMBUS_QUICK;
        args.data = nullptr;

        if (ioctl(file, I2C_SMBUS, &args) == 0)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::SMBusDevice::readByte(uint8_t& value)
{
    bool ret = false;

    if (read(0, SMBUS_BYTE))
    {
        value = data.byte;
        ret = true;
    }
    else
    {
        SF_SMER("error reading byte value");
    }

    return ret;
}

bool OSEF::SMBusDevice::writeByte(const uint8_t& value)
{
    bool ret = false;

    if (write(value, SMBUS_BYTE, nullptr))
    {
        ret = true;
    }
    else
    {
        SF_SMER("error writing byte value " << +value);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeByteQuick(const uint8_t& value)
{
    bool ret = false;

    if (writeQuick(value))
    {
        ret = true;
    }
    else
    {
        SF_SMER("error writing quick byte value " << +value);
    }

    return ret;
}

bool OSEF::SMBusDevice::readByte(const uint8_t& command, uint8_t& value)
{
    bool ret = false;

    if (read(command, SMBUS_BYTE_DATA))
    {
        value = data.byte;
        ret = true;
    }
    else
    {
        SF_SMER("error reading byte " << +command);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeByte(const uint8_t& command, const uint8_t& value)
{
    bool ret = false;

    data.byte = value;
    if (write(command, SMBUS_BYTE_DATA, &data))
    {
        ret = true;
    }
    else
    {
        SF_SMER("error writing value " << +value << " to byte " << +command);
    }

    return ret;
}

bool OSEF::SMBusDevice::readWord(const uint8_t& command, uint16_t& value)
{
    bool ret = false;

    if (read(command, SMBUS_WORD_DATA))
    {
        value = data.word;
        ret = true;
    }
    else
    {
        SF_SMER("error reading word " << +command);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeWord(const uint8_t& command, const uint16_t& value)
{
    bool ret = false;

    data.word = value;
    if (write(command, SMBUS_WORD_DATA, &data))
    {
        ret = true;
    }
    else
    {
        SF_SMER("error writing word " << +command);
    }

    return ret;
}

bool OSEF::SMBusDevice::readWordSwap(const uint8_t& command, uint16_t& value)
{
    bool ret = readWord(command, value);
    value = swap16(value);
    return ret;
}

bool OSEF::SMBusDevice::writeWordSwap(const uint8_t& command, const uint16_t& value)
{
    const bool ret = writeWord(command, swap16(value));
    return ret;
}

bool OSEF::SMBusDevice::readBlock(const uint8_t& command, I2CBlock& values)
{
    bool ret = false;

    if (read(command, SMBUS_BLOCK_DATA))
    {
        if (data.block[0] <= SM_BLOCK_SIZE)
        {
            for (
                    size_t i = 1UL;
                    i <= static_cast<size_t>(data.block[0]);
                    i++)
            {
                values[static_cast<size_t>(i-1)]
                        = data.block[i];
            }

            ret = true;
        }
        else
        {
            SF_SMER("received length error " << +data.block[0] << " > +SM_BLOCK_SIZE");
        }
    }
    else
    {
        SF_SMER("error reading block " << +command);
    }

    return ret;
}

bool OSEF::SMBusDevice::readI2cBlock(const uint8_t& command, const uint8_t& length, I2CBlock& values)
{
    bool ret = false;

    if (length <= SM_BLOCK_SIZE)
    {
        if (read(command, length == SM_BLOCK_SIZE ? SMBUS_I2C_BLOCK_BROKEN : SMBUS_I2C_BLOCK_DATA))
        {
            if (data.block[0] == length)
            {
                for (
                        uint8_t i = 1;
                        (i <= data.block[0])
                        && (i <= SM_BLOCK_SIZE);
                        i++)
                {
                    values[static_cast<size_t>(i-1)] = data.block[static_cast<size_t>(i)];
                }

                ret = true;
            }
            else
            {
                SF_SMER("received length error " << +data.block[0] << " != " << +length);
            }
        }
        else
        {
            SF_SMER("error reading i2c block " << +command << " length " << +length);
        }
    }
    else
    {
        SF_SMER("length too long " << +length << " > " << +SM_BLOCK_SIZE);
    }

    return ret;
}

bool OSEF::SMBusDevice::readI2cBlockData(const uint8_t& command, const uint8_t& length, I2CBlock& values) const
{
    bool ret = false;

    if (length <= SM_BLOCK_SIZE)
    {
        int32_t rlen = i2c_smbus_read_i2c_block_data(file, command, length, &values[0]);
        if (rlen == length)
        {
            ret = true;
        }
        else
        {
            DERR("error reading I²C data block command=" << +command << " errno " << errno << " " << strerror(errno));
        }
    }
    else
    {
        SF_SMER("length too long " << +length << " > " << +SM_BLOCK_SIZE);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeBlock(const uint8_t& command, const uint8_t& length, const I2CBlock& values)
{
    bool ret = false;

    if (length <= SM_BLOCK_SIZE)
    {
        data.block[0] = length;
        for (size_t i=1U; i <= static_cast<size_t>(length); i++)
        {
            data.block[i] = values[static_cast<size_t>(i-1)];
        }

        if (write(command, SMBUS_I2C_BLOCK_DATA, &data))
        {
            ret = true;
        }
        else
        {
            SF_SMER("error writing block " << +command);
        }
    }
    else
    {
        SF_SMER("length too long " << +length << " > " << +SM_BLOCK_SIZE);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeI2cBlock(const uint8_t& command, const uint8_t& length, const I2CBlock& values)
{
    bool ret = false;

    if (length <= SM_BLOCK_SIZE)
    {
        data.block[0] = length;
        for (size_t i=1U; i <= static_cast<size_t>(length); i++)
        {
            data.block[i] = values[static_cast<size_t>(i-1)];
        }

        if (write(command, SMBUS_I2C_BLOCK_BROKEN, &data))
        {
            ret = true;
        }
        else
        {
            SF_SMER("error writing i2c block " << +command);
        }
    }
    else
    {
        SF_SMER("length too long " << +length << " > " << +SM_BLOCK_SIZE);
    }

    return ret;
}

bool OSEF::SMBusDevice::writeI2cBlockData(const uint8_t& command, const uint8_t& length, const I2CBlock& values) const
{
    bool ret = false;

    if (length <= SM_BLOCK_SIZE)
    {
        if (i2c_smbus_write_i2c_block_data(file, command, length, &values[0]) == 0)  // id is sent as block command prefix
        {
            ret = true;
        }
        else
        {
            DERR("error writing I²C data block command=" << +command);
        }
    }
    else
    {
        SF_SMER("length too long " << +length << " > " << +SM_BLOCK_SIZE);
    }

    return ret;
}
