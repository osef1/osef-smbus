#ifndef OSEFSMBUSDEVICE_H
#define OSEFSMBUSDEVICE_H

#include <string>  // std::string

#ifndef SF_SMER
#   ifdef DEBUG
#       define SF_SMER(s) std::cerr << "\033[31;40m" << __LINE__ << " " << __PRETTY_FUNCTION__ << " " << s << " device 0x" << std::hex << +device << std::dec << " adapter " << +adapter << "\033[0;0m" << std::endl
#   else
#       define SF_SMER(s)
#   endif
#endif

namespace OSEF
{
    const uint8_t SM_BLOCK_SIZE = 32;
    typedef uint8_t I2CBlock[SM_BLOCK_SIZE];

    union i2c_smbus_data
    {
        uint8_t byte;
        uint16_t word;
        uint8_t block[SM_BLOCK_SIZE + 2];  // block[0] is used for length and one more for PEC
    };

    struct i2c_smbus_ioctl_data
    {
        uint8_t read_write;
        uint8_t command;
        uint32_t size;
        union i2c_smbus_data *data;
    };

    class SMBusDevice
    {
    public:
        SMBusDevice(const uint8_t& adp, const uint8_t& dvc);
        virtual ~SMBusDevice();

        bool isAdapterOK() const {return (file >= 0);}
        bool isDeviceConnected() const {return deviceConnected;}

        uint8_t getAdapter() const {return adapter;}
        uint8_t getDevice() const {return device;}
        bool displayFunctions(uint32_t& f);

        bool readByte(uint8_t& value);
        bool writeByte(const uint8_t& value);
        bool writeByteQuick(const uint8_t& value);

        bool readByte(const uint8_t& command, uint8_t& value);
        bool writeByte(const uint8_t& command, const uint8_t& value);

        bool readWord(const uint8_t& command, uint16_t& value);
        bool writeWord(const uint8_t& command, const uint16_t& value);

        bool readWordSwap(const uint8_t& command, uint16_t& value);
        bool writeWordSwap(const uint8_t& command, const uint16_t& value);

        bool readBlock(const uint8_t& command, I2CBlock& values);
        bool writeBlock(const uint8_t& command, const uint8_t& length, const I2CBlock& values);

        bool readI2cBlock(const uint8_t& command, const uint8_t& length, I2CBlock& values);
        bool writeI2cBlock(const uint8_t& command, const uint8_t& length, const I2CBlock& values);

        bool readI2cBlockData(const uint8_t& command, const uint8_t& length, I2CBlock& values) const;
        bool writeI2cBlockData(const uint8_t& command, const uint8_t& length, const I2CBlock& values) const;

        SMBusDevice(const SMBusDevice&) = delete;  // copy constructor
        SMBusDevice& operator=(const SMBusDevice&) = delete;  // copy assignment
        SMBusDevice(SMBusDevice&&) = delete;  // move constructor
        SMBusDevice& operator=(SMBusDevice&&) = delete;  // move assignment

    private:
        int file;
        bool deviceConnected;
        union i2c_smbus_data data;

        uint8_t adapter;
        uint8_t device;
        uint32_t functions;

        bool connectDevice();

        bool read(uint8_t command, uint32_t size);
        bool write(uint8_t command, uint32_t size, union i2c_smbus_data* d);
        bool writeQuick(uint8_t value);

        bool checkFunction(const uint32_t& f) const;
        std::string getFunctionName(const uint32_t& f);
        void displayFunction(const uint32_t& f);
    };
}  // namespace OSEF

#endif /* OSEFSMBUSDEVICE_H */
